import logging
import typing

from model import EbmsService, NifiService, FlowFile, MessageEvent, MessageEvents, Message, MessageDataSource
from model import newMessageDatasourceFromMessage

class ListMessageEvents:
    """
    ListMessageEvents collects the events from the embs service and 
    sends each event as a flow file to the NiFi flow file service.
    """

    def __init__(self, embs_service: EbmsService, nifi_service: NifiService):
        self.ebms_service = embs_service
        self.nifi_service = nifi_service

    # TODO: add max message parameter
    def execute(self, messageStatus: str = 'RECEIVED', eventStatus: str = 'RECEIVED'):
        events = self.ebms_service.getMessageEvents(messageStatus, eventStatus)
        for e in events.messageEvents():
            logging.info("event {0}".format(e.id))
            self.nifi_service.putMessageEvent(e)


class FetchMessage:
    """
    FetchMessage fetches the message from the ebms service and sends
    each datasource to the NiFi flow service.
    """
    def __init__(self, embs_service: EbmsService, nifi_service: NifiService):
        self.ebms_service = embs_service
        self.nifi_service = nifi_service

    def execute(self, id: str, processed: bool):
        m = self.ebms_service.getMessage(id, False)
        # Send a flow file for each data source.
        for i in range(len(m.dataSource)):
            self.nifi_service.putMessageContent(
                newMessageDatasourceFromMessage(m, i))

        if processed:
            self.ebms_service.processMessage(m.id)
