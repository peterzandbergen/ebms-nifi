import logging
import sys

import usecase
from adapter.ebms import EbmsSoapService
from adapter.nifi import NifiServiceImpl
from model import NifiService, MessageEvent, MessageDataSource
from adapter.cli import Cli


class MockNifiService(NifiService):
    def __init__(self):
        self.mock = True
        pass

    def putMessageEvent(self, messageEvent: MessageEvent):
        logging.info("MockNifiService.putMessageEvent => id: {0}, status: {1}".format(
            messageEvent.id, messageEvent.eventStatus))

    def putMessageContent(self, message: MessageDataSource):
        logging.info("MockNifiService.putMessageContent => id: {0}, status: {1}, dataSourceIndex: {2}, len(datasource): {3}, fromRole: {4}".format(
            message.id,
            message.messageStatus,
            message.dataSourceIndex,
            len(message.dataSource.content),
            message.fromRole.role,
        ))


def testGetMessageEvents(es: EbmsSoapService, ns: MockNifiService):
    uc = usecase.ListMessageEvents(es, ns)
    # ececute the use case
    uc.execute('RECEIVED', 'RECEIVED')


def testGetMessages(es, ns):
    events = es.getMessageEvents('RECEIVED', 'RECEIVED')
    for event in events.messageEvents():
        uc = usecase.FetchMessage(es, ns)
        uc.execute(event.id, False)
        break

def testPutMessageEvent():
    ns = NifiServiceImpl()
    me = MessageEvent(id="messageEventId", eventStatus="event status test")
    ns.putMessageEvent(me)

def mainTest():
    # Create the embs soap client
    es = EbmsSoapService()
    # ns = MockNifiService()
    nsevents = NifiServiceImpl("http://localhost:19999/nifi/events")
    nsmessages = NifiServiceImpl("http://localhost:19999/nifi/messages")

    testGetMessageEvents(es, nsevents)
    testGetMessages(es, nsmessages)


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

    cli = Cli(sys.argv)
    cli.execute()

    