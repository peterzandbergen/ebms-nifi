from dataclasses import dataclass, field
from typing import List, Tuple, Dict
import typing
import datetime


@dataclass(init=True)
class MessageEvent:
    id: str
    eventStatus: str


@dataclass()
class MessageEvents:
    events: List[MessageEvent]

    def messageEvents(self):
        for m in self.events:
            yield m


@dataclass()
class DataSource:
    contentId: str
    contentType: str
    name: str
    content: bytearray


@dataclass
class Role:
    partyId: str
    role: str


@dataclass()
class Message:
    id: str
    action: str
    conversationId: str
    cpaId: str
    fromRole: Role
    toRole: Role
    messageStatus: str
    refToMessageId: str
    service: str
    timestamp: datetime.datetime
    dataSource: List[DataSource]


@dataclass(init=True)
class MessageDataSource:
    id: str
    action: str
    conversationId: str
    cpaId: str
    fromRole: Role
    toRole: Role
    messageStatus: str
    refToMessageId: str
    service: str
    timestamp: datetime.datetime
    dataSourceIndex: int
    dataSource: DataSource


def newMessageDatasourceFromMessage(m: Message, dataSourceIndex: int) -> MessageDataSource:
    if dataSourceIndex > len(m.dataSource):
        return None

    return MessageDataSource(
        dataSourceIndex=dataSourceIndex,
        dataSource=m.dataSource[dataSourceIndex],
        id=m.id,
        action=m.action,
        conversationId=m.conversationId,
        cpaId=m.cpaId,
        fromRole=m.fromRole,
        toRole=m.toRole,
        messageStatus=m.messageStatus,
        refToMessageId=m.refToMessageId,
        service=m.service,
        timestamp=m.timestamp,
    )


class EbmsService:
    """
    EbmsService acts as an interface for the use cases.
    """

    def getMessageEvents(self, messageStatus: str, eventType: str) -> MessageEvents:
        """
        Returns an array of tuples with the ID and Event type. Array can be empty.
        Throws and exception in case of an error. 
        """
        pass

    def getMessage(self, id: str, processed: bool) -> Message:
        """
        getMessage eturns the message with the given id. 
        If processed is true, then the message status is set to processed when getMessage is succesful.
        Throws and exception if no message with the given id is present.
        """
        pass

    def processMessage(self, id: str):
        """
        Sets the status of message with the given id to processed.
        """
        pass


@dataclass(init=True)
class FlowFile:
    attributes: Dict[str, str]


class NifiService:
    """
    Interface class.
    """

    def putMessageEvent(self, messageEvent: MessageEvent):
        pass

    def putMessageContent(self, message: MessageDataSource):
        pass
