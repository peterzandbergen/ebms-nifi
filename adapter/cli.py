import os
from typing import List, Tuple, Dict
import typing

from adapter.ebms import EbmsSoapService
from adapter.nifi import NifiServiceImpl
from usecase import ListMessageEvents, FetchMessage


class Cli:
    """
    Executes the correct use case based on the passed parameters.

    It uses environment variables for the configuration of the 
    adapters.
    """

    def __init__(self, args):
        self.args = []
        for a in args:
            self.args.append(a)
        wsdl = os.getenv("EBMS_SERVICE_WSDL")
        ep = str(os.getenv("EBMS_SERVICE_ENDPOINT", default=""))
        self.ebms = EbmsSoapService(wsdl, ep)
        self.nifi = NifiServiceImpl(os.getenv("NIFI_EMBS_CALLBACK"))

    def execute(self):
        # Determine the use case to execute.
        if self.args[1].lower() == "ListMessageEvents".lower():
            # Create the use case and execute it
            uc = ListMessageEvents(self.ebms, self.nifi)
            uc.execute()
            pass

        if self.args[1].lower() == "FetchMessage".lower():
            uc = FetchMessage(self.ebms, self.nifi)
            uc.execute(id=self.args[2], processed=bool(self.args[2]))
            pass

    def buildEnv(self):
        pass

# Test command: EBMS_SERVICE_WSDL="http://localhost:18080/ebms-adapter/service/ebms?wsdl" NIFI_EMBS_CALLBACK="http://localhost:19999/nifi/events" python3 main.py listmessageevents
# Test command: EBMS_SERVICE_WSDL="http://localhost:18080/ebms-adapter/service/ebms?wsdl" NIFI_EMBS_CALLBACK="http://localhost:19999/nifi/messages" python3 main.py fetchmessage "f885fe0a-7c99-4ab2-bb01-31648ca2d1fc@localhost" False

if __name__ == "__main__":
    pass
