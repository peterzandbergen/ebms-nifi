from zeep import Client
from typing import List, Tuple, Dict

from model import MessageEvents, MessageEvent, Message, EbmsService, DataSource, Role


class EbmsSoapService(EbmsService):
    """
    Implements the EbmsService interface.
    """

    def __init__(self, wsdl="http://localhost:18080/ebms-adapter/service/ebms?wsdl", endpoint=""):
        """
        init requires that the soap service is active
        """
        # TODO: create types factories
        self.max = 100  # max number of events to return
        self.client = Client(wsdl)
        if endpoint == "":
            self.service = self.client.service
        else:
            self.service = self.client.create_service("", endpoint)

    def getMessageEvents(self, messageStatus: str, eventType: str) -> MessageEvents:
        messageContext_type = self.client.get_type('ns0:ebMSMessageContext')
        messageContext = messageContext_type()
        messageContext.messageStatus = messageStatus
        event_type_type = self.client.get_type('ns0:ebMSMessageEventType')
        event_types = event_type_type(eventType)
        try:
            # todo, create a list of ebms message events
            es = self.service.GetMessageEvents(
                messageContext, event_types, self.max)
            events = []
            for e in es:
                events.append(MessageEvent(e["messageId"], e["type"]))
            return MessageEvents(events)
        except Exception as exc:
            return []
            pass

    def getMessage(self, id: str, processed: bool) -> Message:
        em = self.service.GetMessage(id, processed)
        m = None
        # build data sources list
        ds = []
        for s in em.dataSource:
            ds.append(
                DataSource(
                    contentId=s.contentId,
                    name=s.name,
                    contentType=s.contentType,
                    content=s.content,
                )
            )
        # build message
        m = Message(
            id=em.context.messageId,
            action=em.context.action,
            conversationId=em.context.conversationId,
            cpaId=em.context.cpaId,
            fromRole=Role(
                em.context.fromRole.partyId,
                em.context.fromRole.role),
            messageStatus=em.context.messageStatus,
            refToMessageId=em.context.refToMessageId,
            service=em.context.service,
            timestamp=em.context.timestamp,
            toRole=Role(
                em.context.toRole.partyId,
                em.context.toRole.role),
            dataSource=ds,
        )
        return m

    def processMessage(self, id: str):
        self.service.ProcessMessage(id)
