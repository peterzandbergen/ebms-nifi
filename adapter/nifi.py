from dataclasses import dataclass, field
from typing import List, Tuple, Dict
import datetime

from model import MessageEvent, DataSource, NifiService, MessageDataSource

import requests


class NifiServiceException(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class NifiServiceImpl(NifiService):
    """
    Interface class.
    """

    def __init__(self, callback: str):
        self.url = callback
        self.prefix = "X-Ebms-"

    @classmethod
    def headers(cls):
        return {
            "Content-Type": "application/octet-stream",
        }

    def hprefix(self, h: str) -> str:
        return self.prefix + h

    def putMessageEvent(self, messageEvent: MessageEvent):
        headers = self.headers()
        headers[self.hprefix("messageId")] = messageEvent.id
        headers[self.hprefix("eventStatus")] = messageEvent.eventStatus

        resp = requests.post(self.url, headers=headers)
        if resp.status_code != 200:
            raise(NifiServiceException("error calling nifi callback"))

    def putMessageContent(self, message: MessageDataSource):
        headers = self.headers()
        headers[self.hprefix("messageId")] = message.id
        headers[self.hprefix("action")] = message.action
        headers[self.hprefix("cpaId")] = message.cpaId
        headers[self.hprefix("messageStatus")] = message.messageStatus
        headers[self.hprefix("refToMessageId")] = message.refToMessageId
        headers[self.hprefix("timestamp")] = message.timestamp.replace(
            tzinfo=datetime.timezone.utc).isoformat()
        headers[self.hprefix("conversationId")] = message.conversationId
        headers[self.hprefix("fromRole-partyId")] = message.fromRole.partyId
        headers[self.hprefix("fromRole-role")] = message.fromRole.role
        headers[self.hprefix("toRole-partyId")] = message.toRole.partyId
        headers[self.hprefix("toRole-role")] = message.toRole.role
        headers[self.hprefix("service")] = message.service
        headers[self.hprefix("dataSourceIndex")] = str(message.dataSourceIndex)
        headers[self.hprefix("dataSource-content-contentId")
                ] = message.dataSource.contentId
        headers[self.hprefix("dataSource-content-contentType")
                ] = message.dataSource.contentType
        headers[self.hprefix("dataSource-content-name")
                ] = message.dataSource.name

        resp = requests.post(self.url, headers=headers,
                             data=message.dataSource.content)
        if resp.status_code != 200:
            raise(NifiServiceException("error calling nifi callback"))
